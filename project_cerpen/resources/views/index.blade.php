<!DOCTYPE html>
<html>
<head>
	<title>Cerpen Web</title>
</head>
<body>

	<h3>Data Cerpen</h3>

	<a href={{ route('plus') }}> + Tambah Cerpen Baru</a>

	<br/>
	<br/>

	<table border="1">
		<tr>
			<th>Nama</th>
			<th>Judul</th>
			<th>Isi Cerpen</th>
		</tr>
		@foreach($cerpen as $c)
		<tr>
			<td>{{$c->id}}
			<td>{{ $c->nama }}</td>
			<td>{{ $c->judul }}</td>
            <td>{{ $c->isi_cerpen  }}</td>
			<td>
				<a href="/edited{{ $c->id }}">Edit</a>
				|
				<a href="/delete{{ $c->id }}">Hapus</a>
			</td>
		</tr>
		@endforeach
	</table>


</body>
</html>


