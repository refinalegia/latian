<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class LoginController extends Controller
{

    public function login()
    {
        if (Auth::check()) {
            return redirect('/home');
        }else{
            return view('login');
        }
    }


    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'username' => 'required',
            'password' => 'required'        ]);

        if(Auth::attempt($credentials)) {

            //$request->session->regenerate();
            return redirect()->intended('/home');
        }

        return back()->with('LoginError', 'Login failed!');

    }

    public function actionlogout()
    {
        Auth::logout();
        return redirect('/');
    }
}
