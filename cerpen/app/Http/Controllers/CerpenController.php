<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Cerpen;


class CerpenController extends Controller
{
    public function index()
    {
    	// mengambil data dari table cerpen
    	$cerpen = DB::table('cerpen')->get();

    	// mengirim data cerpen ke view index
    	return view('index',['cerpen' => $cerpen]);

    }

    // method untuk menampilkan view form tambah cerpen
public function tambah()
    {

	// memanggil view tambah
	return view('tambah');

    }


    // method untuk insert data ke table cerpen
public function store(Request $request)
{
	// insert data ke table cerpen
	DB::table('cerpen')->insert([
        'id'=>$request->id,
		'nama' => $request->nama,
        'isi_cerpen' => $request->isi_cerpen,
        'judul' => $request->judul
	]);
	// alihkan halaman ke halaman cerpen
	return redirect('/cerpen');

}

// method untuk edit data cerpen
public function edit($id)
{
	// mengambil data cerpen berdasarkan id yang dipilih
	$cerpen = DB::table('cerpen')->where('id',$id)->get();
	// passing data cerpen yang didapat ke view edit.blade.php
	return view('edit',['cerpen' => $cerpen]);
}

// update data cerpen
public function update(Request $request)
{
	// update data cerpen
	DB::table('cerpen')->where('id',$request->id)->update([
        'id' => $request->id,
        'nama' => $request->nama,
        'isi_cerpen' => $request->isi_cerpen,
        'judul' => $request->judul
	]);
	// alihkan halaman ke halaman cerpen
	return redirect('/cerpen');
}

// method untuk hapus data cerpen
public function hapus($id)


{
	// menghapus data cerpen berdasarkan id yang dipilih
	DB::table('latian')->where('id',$id)->delete();

	// alihkan halaman ke halaman cerpen
	return redirect('/cerpen');
}
}

