<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function register()
    {
            return view('register', [
                'title' => 'Register'
            ]);
    }

    public function store(Request $request)
    {
                $validatedData = $request->validate([
                'id' => 'required',
                'name' => 'required|max: 255',
                'username' => ['required', 'min:3', 'max:255', 'unique:users'],
                'password' => 'required|min:6|max: 255',
            ]);

            $validatedData['password'] = Hash::make($validatedData['password']);

            User::create($validatedData);
            $request->session()->flash('success', 'Success!! Please Login');

            return redirect('/login');

    }
}


