@extends('layouts.auth')
@section('content')

    <body>
        <div class="container h-100">
            <div class="d-flex justify-content-center h-100">
                <div class="user_card">
                    <div class="d-flex justify-content-center">
                        <div class="brand_logo_container">
                            <img src="{{ asset('images/logo.jpg') }}" class="brand_logo" alt="Logo">
                        </div>
                    </div>
                    <div class="d-flex justify-content-center form_container">
                        <form action="{{ route('register') }}" method="post">
                            @csrf
                            <div class="row px-5">
                                <h5 class="d-flex justify-content-center login_container ml-1 px-2">REGISTRATION FORM</h5>
                                <div class="col-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                                        </div>
                                        <input type="text" name="name" id="name"
                                            class="form-control input_user @error('name') is-invalid @enderror" value="{{ old('name') }}"
                                            placeholder="Name" required>
                                    </div>
                                    @error('name')
                                        <div class="mt-n3 text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                                        </div>
                                        <input type="text" name="username" id="username"
                                            class="form-control input_user @error('username') is-invalid @enderror" value="{{ old('username') }}"
                                            placeholder="Username" required>
                                    </div>
                                    @error('username')
                                        <div class="mt-n3 text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <div class="input-group mb-2">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                                        </div>
                                        <input type="password" name="password" id="password"
                                            class="form-control input_pass @error('password') is-invalid @enderror" value=""
                                            placeholder="Password" required>
                                    </div>
                                    @error('password')
                                        <div class="mt-n2 text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="d-flex justify-content-center mt-3 login_container px-5">
                                <button type="submit" class="btn login_btn">Registrasi</button>
                            </div>
                            <div class="mt-4">
                                <div class="d-flex justify-content-center links">
                                    Sudah memiliki akun? <a href="{{ route('login') }}" class="ml-2">Login
                                        sekarang</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
@endsection
