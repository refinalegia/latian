@extends('layouts.auth')
@section('content')

    <body>
        <div class="container h-100">
            <div class="d-flex justify-content-center h-100">
                <div class="user_card">
                    <div class="d-flex justify-content-center">
                        <div class="brand_logo_container">
                            <img src="{{ asset('images/logo.jpg') }}" class="brand_logo" alt="Logo">
                        </div>
                    </div>
                    <div class="d-flex justify-content-center form_container">
                        <form action="{{ route('login') }}" method="post">
                            @csrf
                            @if(session()->has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('success') }}
                                    <button type="text" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                                </div>
                            @endif
                            @if(session()->has('LoginError'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{ session('LoginError') }}
                                    <button type="text" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                                </div>
                            @endif
                            <div class="row px-5">
                                <div class="col-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                                        </div>
                                        <input type="text" name="username" id="username"
                                            class="form-control input_user @error('username') is-invalid @enderror" value=""
                                            placeholder="Username" required>
                                    </div>
                                    @error('username')
                                        <div class="mt-n3 text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <div class="input-group mb-2">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                                        </div>
                                        <input type="password" name="password" id="password"
                                            class="form-control input_pass @error('password') is-invalid @enderror" value=""
                                            placeholder="Password" required>
                                    </div>
                                    @error('password')
                                        <div class="mt-n2 text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="d-flex justify-content-center mt-3 login_container px-5">
                                <button type="submit" class="btn login_btn">Login</button>
                            </div>
                            <div class="mt-4">
                                <div class="d-flex justify-content-center links">
                                    Tidak memiliki akun? <a href="{{ route('register') }}" class="ml-2">Daftar
                                        sekarang</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
@endsection
