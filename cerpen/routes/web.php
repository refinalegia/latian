<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\CerpenController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//menampilkan form login
Route::get('/login', [LoginController::class, 'login'])->name('login');
//authenticate
Route::post('/login', [LoginController::class, 'authenticate'])->name('authenticate');



//menampilkan halaman home menggunakan middleware auth agar halaman home hanya bisa diakses ketika melakukan login aja
Route::get('/home', [HomeController::class, 'index'])->name('home')->middleware('auth');
//melakukan logout, pake middleware dengan alasan yg sama juga
Route::get('/actionlogout', [LoginController::class, 'actionlogout'])->name('actionlogout')->middleware('auth');

Route::get('/register', [RegisterController::class, 'register'])->name('register');
Route::post('/register', [RegisterController::class, 'store'])->name('register');


Route::get('/cerpen', [CerpenController::class, 'index'])->name('awal');
//Route::get('cerpen/tambah', 'CerpenController@tambah');
Route::get('/cerpen/tambah', [CerpenController::class, 'tambah'])->name('plus');

//mengirimkann data melalui form store
Route::post('/cerpen/tambah', [CerpenController::class, 'store'])->name('plus');

Route::get('/cerpen/edit/{id}', [CerpenController::class, 'edit'])->name('edited');
Route::post('/cerpen/edit/{id}', [CerpenController::class, 'update'])->name('ganti');

Route::post('/cerpen/hapus/{id}', [CerpenController::class, 'index'])->name('delete');
